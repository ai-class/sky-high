﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHandler : MonoBehaviour
{
    //System Variables
    static public GameHandler instance;                         //a public static instance of the game handler that can be accessed by everyone

    public float roomTransitionTime = 1f;                       //how long it takes to transition from one room to another

    //player variables
    [SerializeField] int maxHPStarting = 3;                     //maximum HP of the player when the game starts

    [SerializeField] float walkSpeedStarting = 2.5f;
    [SerializeField] float runSpeedStarting = 3f;
    [SerializeField] float sprintSpeedStarting = 4f;

    [SerializeField] bool canSprintStarting = false;

    //insert other player variables

    //Camera Variables
    [SerializeField] public Camera mainCamera;                  //the main camera that the player views
    Vector3 mainCameraOffset = new Vector3(0, 12, -2.1f);
    Vector3 mainCameraEulerAngels = new Vector3(80, 0, 0);
    [SerializeField] public Camera miniMapCamera;               //the camera that is controlls movement of the minimap
    Vector3 miniMapCameraOffset = new Vector3(0, 60f, 0);
    Vector3 miniMapCameraEulerAngels = new Vector3(90, 0, 0);

    //Object variables
    [SerializeField] MapHandler mapHandler;                     //the map handler object that needs to be referenced
    PlayerHandler playerStats;
    GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        Initilize();
        //ensure this object cannot be destroyed in a new scene
        DontDestroyOnLoad(this);
    }

    //initilize the Game Handler
    void Initilize()
    {
        LinkInstance();
        MapSetup();
        PlayerSetup();
    }

    //create a singleton instance of the game handler, do not overwrite if there is one currently
    void LinkInstance()
    {
        if (instance == null)
            instance = this;
    }

    //create the map and move the cameras to the correct starting location
    void MapSetup()
    {
        //create the floor
        mapHandler.Initilize();

        //move the cameras to the starting location
        MoveCameraToRoom(mainCamera, mapHandler.currentRoom.transform);
        MoveCameraToRoom(miniMapCamera, mapHandler.currentRoom.transform);
    }

    //create the player 
    void PlayerSetup()
    {
        InstantiatePlayer();
    }

    //moves the camera instatnty to the designated location
    public void MoveCameraToRoom(Camera cameraBeingMoved, Transform roomTransform)
    {
        Vector3 destination = roomTransform.position;
        Vector3 eulerAngles = new Vector3(0, 0, 0);

        if (cameraBeingMoved == mainCamera)
        {
            destination += mainCameraOffset;
            eulerAngles += mainCameraEulerAngels;
        }
        else if (cameraBeingMoved == miniMapCamera)
        {
            destination += miniMapCameraOffset;
            eulerAngles += miniMapCameraEulerAngels;
        }
        else
        {
            Debug.Log("ERROR: unknown camera: " + cameraBeingMoved.ToString() + "is trying to be moved to a room");
            return;
        }

        cameraBeingMoved.transform.position = destination;
        cameraBeingMoved.transform.localEulerAngles = eulerAngles;
    }

    //transitions a camera movemment to a specified room based on the room transition time
    IEnumerator TransitionCameraToRoom(Camera cameraBeingMoved, Transform roomTransform)
    {
        Vector3 destination = roomTransform.position;
        Vector3 eulerAngles = new Vector3(0, 0, 0);
        Vector3 currentPos = cameraBeingMoved.transform.position;
        Vector3 currentEuler = cameraBeingMoved.transform.eulerAngles;

        //get the correct values based on the camera argument
        if (cameraBeingMoved == mainCamera)
        {
            destination += mainCameraOffset;
            eulerAngles += mainCameraEulerAngels;
        }
        else if (cameraBeingMoved == miniMapCamera)
        {
            destination += miniMapCameraOffset;
            eulerAngles += miniMapCameraEulerAngels;
        }
        else
        {
            Debug.Log("ERROR: unknown camera: " + cameraBeingMoved.ToString() + "is trying to transition using roomTransitionCamera");
            yield return null;
        }

        float percentageCompleted = 0f;

        while (percentageCompleted < 1)
        {
            percentageCompleted += Time.deltaTime / roomTransitionTime;
            cameraBeingMoved.transform.position = Vector3.Lerp(currentPos, destination, percentageCompleted);
            cameraBeingMoved.transform.eulerAngles = Vector3.Lerp(currentEuler, eulerAngles, percentageCompleted);
            yield return null;
        }
    }

    //transition the player from one room to another
    public void RoomTransition(WallNode wallNodeDestination)
    {
        //gather variables
        Vector3 destination = wallNodeDestination.transform.position;
        Vector3 destinationOffset = wallNodeDestination.transform.forward * 1.4f;
        Vector3 modifiedDestination = destination + destinationOffset;
        GameObject oldCurrentRoom = mapHandler.currentRoom;
        GameObject newCurrentRoom = wallNodeDestination.GetComponentInParent<RoomNode>().gameObject;

        //start the room transition coroutine
        StartCoroutine(RoomTransitionRoutine(oldCurrentRoom, newCurrentRoom, modifiedDestination));
    }

    //disables the player then transitions him from one room to another
    IEnumerator RoomTransitionRoutine(GameObject oldCurrentRoom, GameObject newCurrentRoom, Vector3 playerDestination)
    {
        //deactivate the old room
        oldCurrentRoom.GetComponent<RoomNode>().DeactivateRoom();
        //assign the new room as the current room
        mapHandler.currentRoom = newCurrentRoom;
        //have the cameras transition over to the next room
        StartCoroutine(TransitionCameraToRoom(mainCamera, mapHandler.currentRoom.transform));
        StartCoroutine(TransitionCameraToRoom(miniMapCamera, mapHandler.currentRoom.transform));
        //transition the player to the new room position
        player.GetComponent<PlayerHandler>().TransitionPlayerToRoomPosition(playerDestination);
        //wait for the transiton to finish
        yield return new WaitForSeconds(roomTransitionTime);

        //activate the current room
        mapHandler.currentRoom.GetComponent<RoomNode>().ActivateRoom();
        yield return null;
    }


    //move the player armiture though the player handler method
    public void MovePlayerPosition(Vector3 destination)
    {
        player.GetComponent<PlayerHandler>().MovePlayerPosition(destination);
    }

    //creates a copy of the player with the stats initilized from playerStats
    public void InstantiatePlayer()
    {
        player = Instantiate(PrefabContainer.instance.player, new Vector3(0, 0, 0), PrefabContainer.instance.player.transform.rotation);
        playerStats = player.GetComponent<PlayerHandler>();
        playerStats.Initilize(maxHPStarting, walkSpeedStarting, runSpeedStarting, sprintSpeedStarting, canSprintStarting);
        ResetPlayerPosition();
    }

    //reset the player position to the begining room
    void ResetPlayerPosition()
    {
        playerStats.MovePlayerPosition(mapHandler.currentRoom.transform.position);
    }

    //load the next floor
    public void LoadNextFloor()
    {
        int floorNum = mapHandler.floorNumber;
        int random = 3;
        if (3 - floorNum < 0)
            random = 0;
        else
            random = 3 - floorNum;

        mapHandler.DestroyCurrentFloor();
        playerStats.SetPlayerModelActive(false);

        mapHandler.CreateNewFloor(5 + (int)(floorNum / 2 ), 5 + (int)(floorNum / 2), 
            Random.Range(random, 5 + (int)(floorNum / 2)), 4 + floorNum, PrefabContainer.instance.rooms, PrefabContainer.instance.walls);

        ResetPlayerPosition();
        playerStats.SetPlayerModelActive(true);

        //move the cameras to the starting location
        MoveCameraToRoom(mainCamera, mapHandler.currentRoom.transform);
        MoveCameraToRoom(miniMapCamera, mapHandler.currentRoom.transform);

    }

}
