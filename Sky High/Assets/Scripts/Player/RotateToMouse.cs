﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateToMouse : MonoBehaviour
{
    // This script will rotate whatever it's attached to, to face the cursor,

    public Camera cam;
    private float maxRayLength = 100;

    private Ray mouseRay;
    private RaycastHit rayHit;

    private Vector3 pos;
    private Vector3 desiredDirection;
    private float offsetX;
    private float offsetY;
    private float offsetZ;
    private Quaternion desiredRotation;


    // Start is called before the first frame update
    void Start()
    {
        if (GameHandler.instance == null && cam == null)
            Debug.Log("ERROR: RotateToMouse cannot find an instance of the GameHandler");
        else if(cam == null)
            cam = GameHandler.instance.mainCamera;
    }

    // Update is called once per frame
    void Update()
    {
        //update the position of the object to the parent
        transform.position = transform.parent.GetChild(0).transform.position;

        var mousePos = Input.mousePosition;
        mouseRay = cam.ScreenPointToRay(mousePos);
        if(Physics.Raycast(mouseRay.origin, mouseRay.direction, out rayHit, maxRayLength))
        {
            RotateObjectToMouseDirection(gameObject, rayHit.point);
        }
        else
        {
            var pos = mouseRay.GetPoint(maxRayLength);
            RotateObjectToMouseDirection(gameObject, rayHit.point);
        }
    }

    void RotateObjectToMouseDirection(GameObject obj, Vector3 destination)
    {
        destination.y = 0;
        desiredDirection = destination - obj.transform.position;
        desiredRotation = Quaternion.LookRotation(desiredDirection);
        desiredRotation.x = 0;
        desiredRotation.z = 0;

        //Quaternion desiredRoatationOffset = desiredRotation * Quaternion.Inverse(transform.parent.rotation);

        obj.transform.localRotation = Quaternion.Lerp(transform.rotation, desiredRotation, 1);
    }
}
