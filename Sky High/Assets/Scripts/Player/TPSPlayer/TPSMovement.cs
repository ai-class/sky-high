﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TPSMovement : MonoBehaviour
{
    public string HorizontialInputName;
    public string VerticalInputName;
    public float MoveSpeed = 8;

    private CharacterController CharController;
    private Rigidbody rb;
    private Vector3 movement;

    // Start is called before the first frame update
    void Awake()
    {
        CharController = GetComponent<CharacterController>();
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //PlayerMovement();
    }

    void FixedUpdate()
    {
        PlayerMovement();
        turning();
    }


    public void PlayerMovement()
    {
        float HorizInput = Input.GetAxisRaw(HorizontialInputName);
        float VertInput = Input.GetAxisRaw(VerticalInputName);

        movement.Set(HorizInput, 0, VertInput);
        movement = movement.normalized * MoveSpeed * Time.deltaTime;
        rb.MovePosition(transform.position + movement);
    }

    void turning()
    {
        Ray cameRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit rayHit;

        if (Physics.Raycast(cameRay, out rayHit, 100f))
        {
            Vector3 playerToMouse = rayHit.point - transform.position;
            playerToMouse.y = 0f;

            Quaternion newRot = Quaternion.LookRotation(playerToMouse);
            rb.MoveRotation(newRot);
        }
    }
}
