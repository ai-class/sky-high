﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballScript : MonoBehaviour
{
    //public GameObject hitFX;
    public int damage = 50;
    public float speed = 30;
    public float activeTime = 3;


    public bool fireType = false;
    public bool iceType = false;
    public bool lightningType = false;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("destroyPrefab", activeTime);
    }

    // Update is called once per frame
    void Update()
    {    
        transform.position += transform.forward * (speed * Time.deltaTime);   
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            if (fireType)
            {
                other.GetComponent<enemyHealth>().takeFireDamage(damage);
                destroyPrefab();
            }
            else if (iceType){
            }
            else if (lightningType){
            }
            else
            {
                other.GetComponent<enemyHealth>().takeDamage(damage);
                destroyPrefab();
            }
        }
    }

    void destroyPrefab()
    {
        //Instantiate(hitFX, gameObject.transform);
        Destroy(gameObject);
    }
}
