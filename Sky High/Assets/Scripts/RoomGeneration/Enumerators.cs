﻿public enum Direction { NORTH, SOUTH, EAST, WEST };
public enum WallType { DOOR, WALL, BARS };
public enum RoomType { START, NORMAL, ITEM, SHOP, BOSS };