﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public struct Coordinates
{
    //x and y coordinate variables
    public int x, y;

    //coodinate contructor. Must take X and Y values when constructed
    public Coordinates(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    //making your coordinates a string will return this format
    public override string ToString()
    {
        return string.Format("({0},{1})", x, y);
    }

    //change your coordinate value to a vector2
    public Vector2 ToVector2()
    {
        return new Vector2(x, y);
    }

    public static float GetDistance(Coordinates firstCoordinates, Coordinates secondCoordinates)
    {
        return Vector2.Distance(firstCoordinates.ToVector2(), secondCoordinates.ToVector2());
    }

    public static Direction FaceDirection(Coordinates startCoordinates, Coordinates endCoordinates)
    {
        Direction directionX = Direction.EAST;
        Direction directionY = Direction.NORTH;
        int differenceX = -1, differenceY = -1;

        if (startCoordinates.x > endCoordinates.x)
        {
            directionX = Direction.WEST;
            differenceX = startCoordinates.x - endCoordinates.x;
        }
        else if (startCoordinates.x < endCoordinates.x)
        {
            directionX = Direction.EAST;
            differenceX = endCoordinates.x - startCoordinates.x;
        }

        if (startCoordinates.y > endCoordinates.y)
        {
            directionY = Direction.SOUTH;
            differenceY = startCoordinates.y - endCoordinates.y;
        }
        else if (startCoordinates.y < endCoordinates.y)
        {
            directionY = Direction.NORTH;
            differenceY = endCoordinates.y - startCoordinates.y;
        }

        if (differenceX > differenceY)
        {
            return directionX;
        }
        else if (differenceY > differenceX)
        {
            return directionY;
        }
        else if ((differenceX == differenceY) && differenceX != -1)
        {
            if (Random.Range(0, 1) == 0)
                return directionX;
            else
                return directionY;
        }


        Debug.Log("Cannot face any specific direction, defaulting to north");
        return Direction.NORTH;
    }

    public Coordinates GetAdjacentCoordinate(Direction direction)
    {
        int x = this.x;
        int y = this.y;
        switch (direction)
        {
            case Direction.NORTH:
                y += 1;
                break;
            case Direction.EAST:
                x += 1;
                break;
            case Direction.SOUTH:
                y -= 1;
                break;
            case Direction.WEST:
                x -= 1;
                break;
            default:
                break;
        }

        return new Coordinates(x, y);
    }

    public static Direction GetOppositeDirection(Direction direction)
    {
        switch (direction)
        {
            case Direction.NORTH:
                return Direction.SOUTH;
            case Direction.EAST:
                return Direction.WEST;
            case Direction.SOUTH:
                return Direction.NORTH;
            case Direction.WEST:
                return Direction.EAST;
            default:
                Debug.Log("Error, No direction has been set. Cannot find opposite direction");
                return 0;
        }
    }

}
