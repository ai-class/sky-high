﻿using System.Collections;
using System;

public class PsuedoQueue<T> where T : IComparable
{
    private ArrayList objList = new ArrayList();

    public int Length
    {
        get { return objList.Count; }
    }

    public bool Contains(object obj)
    {
        return objList.Contains(obj);
    }

    public T GetFirstObject()
    {
        if (objList.Count > 0)
        {
            return (T)objList[0];
        }
        return default;
    }

    public void Push(T obj)
    {
        objList.Add(obj);
        objList.Sort();
    }

    public void Remove(T obj)
    {
        objList.Remove(obj);
        objList.Sort();
    }

}